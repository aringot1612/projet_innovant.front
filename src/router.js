import Home from './components/Home.vue';
import Room from './components/Room.vue';

import VueRouter from 'vue-router';

/** Création du router. */
export default new VueRouter({
  mode: 'history',
  routes: [
    {path: "/", component: Home},
    {path: "/room/:number", component: Room}
  ]
});