import axios from "axios";

const genericCall = {
	method: 'GET',
	url: 'undefined',
	headers: {
		'Accept': 'application/json'
	},
	validateStatus: () => true
};

export async function getData(id = 0){
	let call = genericCall;
	return new Promise(function(resolve, reject){
		call.url = process.env.VUE_APP_API_URL + 'api/projetinnovant/front/' + id;
		try {
			axios.request(call).then((response) =>{
				if(response.status != 200)
					reject(response.status);
				else
					resolve(response.data);
			});
		} catch (error) {
			reject(error);
		}
	});
}